package main

import (
	"bytes"
	"encoding/json"
	"flag"
	"fmt"
	"io"
	"log"
	"net/http"
	"time"

	"github.com/gorilla/mux"

	"hdps.com/pkg/discover"
	"hdps.com/pkg/logging"
	"hdps.com/pkg/tools/dbtools"
	"hdps.com/pkg/tools/helpers"
	"hdps.com/pkg/tools/services"
	"hdps.com/pkg/tools/structs"
)

var (
	config       discover.DiscoverConfig
	servicesList structs.ServicesList

	cacheServicesDb *dbtools.LevelDB

	logs      services.Logger
	infoQueue = make(chan structs.ServiceConfig, 1000)
)

// Service runner function
func main() {
	var configPath, cacheServices string
	flag.StringVar(&configPath, "config", "configs/services/discover.json", "path to config file")
	flag.StringVar(&cacheServices, "cache", "storage/discover_services", "path to service items cache")
	flag.Parse()

	if !helpers.LoadConfiguration(configPath, &config) {
		log.Printf("Load discover config error: %s\n", configPath)
		return
	}
	config.UseEnv(structs.ParseEnvConfig())

	cacheServicesDb = &dbtools.LevelDB{Name: cacheServices}
	cacheServicesDb.Open()

	serviceData := structs.ServiceConfig{
		Name:         "Discover 1",
		Type:         "discover",
		Host:         config.Host,
		ExternalHost: config.ExternalHost,
		Port:         config.Port,
		Scheme:       config.Scheme,
		Status:       true,
	}
	servicesList = structs.CreateServicesList()
	servicesList.Add(serviceData)
	logs = logging.NewLogger(config.Type, &servicesList)

	data, err := cacheServicesDb.Get([]byte("services"))
	if err == nil {
		helpers.JsonDecode(data, &servicesList)
	}

	go logs.Loop()
	go Worker()
	StartServer()
}

// Check service state and save result to storage
func checkService(info structs.ServiceConfig) error {
	bytesData := new(bytes.Buffer)
	json.NewEncoder(bytesData).Encode(servicesList.GetData())
	resp, err := http.Post(fmt.Sprintf("%s/services", info.GetExternalUrl()), "application/json; charset=utf-8", bytesData)
	// fmt.Println("RES:", info.GetExternalUrl(), resp)
	if err != nil {
		logs.Error("Check service error: %s %v", info.Name, err)
		servicesList.SetStatus(info.Name, false)
		// servicesList.Remove(info)
		return err
	}
	defer resp.Body.Close()

	servicesList.SetStatus(info.Name, true)
	// var remoteServicesList map[string][]structs.ServiceConfig
	// if err := json.NewDecoder(resp.Body).Decode(&remoteServicesList); err != nil {
	//     logs.Error("Decode services list error: %s %v", info.Name, err)
	//     return nil
	// }

	return nil
}

// Run services update and sync data between them
func updateServices() {
	// get services
	items := servicesList.GetData()

	// check services list
	for _, v := range items {
		for _, elm := range v {
			// send request to each service
			logs.Debug("Check service %v", elm)
			checkService(elm)
		}
	}

	cacheServicesDb.Put([]byte("services"), helpers.JsonEncode(servicesList))
}

// main worker function
func Worker() {
	RefreshTimer := time.NewTicker(time.Duration(config.RefreshTimeout) * time.Second)

	for {
		select {

		case info := <-infoQueue:
			// pass info to storage
			servicesList.Add(info)

		case <-RefreshTimer.C:
			go updateServices()
		}
	}
}

// http server runner
func StartServer() {
	router := mux.NewRouter()
	router.HandleFunc("/get", helpers.LoggerFunc(GetAllServices, "GetAllServices")).Methods("GET")
	router.HandleFunc("/get/{type}", helpers.LoggerFunc(GetService, "GetService")).Methods("GET")
	router.HandleFunc("/put/{type}", helpers.LoggerFunc(PutService, "PutService")).Methods("POST")

	router.HandleFunc("/health", HealthCheck).Methods("GET")
	router.HandleFunc("/full-info", HealthCheck).Methods("GET")
	router.HandleFunc("/services", discover.ServicesListWrapper(&servicesList)).Methods("POST")

	server := http.Server{
		Addr:    fmt.Sprintf("%s:%d", config.Host, config.Port),
		Handler: router,
	}
	log.Println(fmt.Sprintf("%s bind to %s:%d", config.Type, config.Host, config.Port))
	server.ListenAndServe()
}

// Put service info to internal storage
func PutService(w http.ResponseWriter, r *http.Request) {
	if r.Body == nil {
		http.Error(w, `{"status":false,"error":"Invalid request"}`, 400)
		return
	}
	defer r.Body.Close()

	// a, _ := ioutil.ReadAll(r.Body)
	// log.Println(string(a))
	var configStruct structs.ServiceConfig
	if err := json.NewDecoder(r.Body).Decode(&configStruct); err != nil {
		logs.Error("Failed to decode config json: %v", err)
		http.Error(w, fmt.Sprintf(`{"status":false,"error":"Decode json error: %s"}`, err), 400)
		return
	}
	configStruct.Status = true
	logs.Debug("Input config struct %v", configStruct)

	infoQueue <- configStruct

	w.Header().Set("Content-Type", "application/json")
	// w.WriteHeader(http.StatusOK)
	w.Write(helpers.JsonEncode(servicesList.GetData()))
}

// Get service info from storage
func GetService(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	serviceType := params["type"]

	w.WriteHeader(http.StatusOK)

	service := servicesList.Get(serviceType)
	if service == nil {
		io.WriteString(w, "0")
		return
	}

	// w.Header().Set("Content-Type", "application/json")
	w.Write(helpers.JsonEncode(service))
}

// Get all services
func GetAllServices(w http.ResponseWriter, r *http.Request) {
	// w.WriteHeader(http.StatusOK)
	w.Header().Set("Content-Type", "application/json")
	w.Write(helpers.JsonEncode(servicesList.GetData()))
}

// Check service status
func HealthCheck(w http.ResponseWriter, r *http.Request) {
	data := make(map[string]interface{})

	data["data"] = helpers.GetServiceInfo()
	data["services"] = servicesList.GetData()

	// w.WriteHeader(http.StatusOK)
	w.Header().Set("Content-Type", "application/json")
	w.Write(helpers.JsonEncode(data))
}
