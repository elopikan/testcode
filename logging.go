package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
	"time"

	"github.com/gorilla/mux"

	"hdps.com/pkg/discover"
	"hdps.com/pkg/logging"
	"hdps.com/pkg/tools/helpers"
	"hdps.com/pkg/tools/structs"
)

var (
	config       logging.LoggerConfig
	servicesList structs.ServicesList

	messagesLog   *logging.MessagesLog
	messagesQueue = make(chan logging.LogMessage, 100000)
)

// Service runner function
func main() {
	var configPath string
	flag.StringVar(&configPath, "config", "configs/services/logger.json", "path to config file")
	flag.Parse()

	if !helpers.LoadConfiguration(configPath, &config) {
		log.Printf("Load logger config error: %s\n", configPath)
		return
	}
	config.FilePath = configPath
	config.UseEnv(structs.ParseEnvConfig())

	// hook for compile globs
	config.Reload()

	servicesList = structs.CreateServicesList()
	messagesLog = logging.CreateMessagesLog()

	go Worker()
	go func() {
		time.Sleep(500 * time.Millisecond)
		discover.LoadServices(config.DiscoverUrl, config.Type, config, &servicesList)
	}()
	StartServer()
}

// write receiver message to storage
func handleMessage(message logging.LogMessage) {
	handlers := config.Handlers.GetHandlers(message)
	for _, h := range handlers {
		f, err := os.OpenFile(h.FilePath, os.O_APPEND|os.O_WRONLY|os.O_CREATE, 0644)
		if err != nil {
			log.Println(err)
			continue
		}

		defer f.Close()
		t := time.Now()
		data := fmt.Sprintf("%s [%s] %s: %s\n", t.Format("2006/01/02 15:04:05"),
			message.GetLevelString(), message.Sender, message.Text)
		messagesLog.Add(h.Pattern, data)
		if _, err = f.WriteString(data); err != nil {
			continue
		}
	}
}

// main worker function
func Worker() {
	ReloadTimer := time.NewTicker(time.Duration(config.ReloadTimeout) * time.Second)
	DiscoverTimer := time.NewTicker(time.Duration(config.DiscoverTimeout) * time.Second)

	for {
		select {

		case msg := <-messagesQueue:
			// handle input message
			handleMessage(msg)

		case <-ReloadTimer.C:
			go config.Reload()

		case <-DiscoverTimer.C:
			go discover.LoadServices(config.DiscoverUrl, config.Type, config, &servicesList)
		}
	}
}

// http server runner
func StartServer() {
	router := mux.NewRouter()
	// router.HandleFunc("/", helpers.LoggerFunc(ReceiveMessages, "ReceiveMessages")).Methods("POST")
	router.HandleFunc("/", ReceiveMessages).Methods("POST")

	router.HandleFunc("/health", HealthCheck).Methods("GET")
	router.HandleFunc("/full-info", FullInfo).Methods("GET")
	router.HandleFunc("/services", discover.ServicesListWrapper(&servicesList)).Methods("POST")

	server := http.Server{
		Addr:    fmt.Sprintf("%s:%d", config.Host, config.Port),
		Handler: router,
	}
	log.Println(fmt.Sprintf("%s bind to %s:%d", config.Name, config.Host, config.Port))
	server.ListenAndServe()
}

// Put service info to internal storage
func ReceiveMessages(w http.ResponseWriter, r *http.Request) {
	if r.Body == nil {
		http.Error(w, `{"status":false,"error":"Invalid request"}`, 400)
		return
	}
	defer r.Body.Close()

	// a, _ := ioutil.ReadAll(r.Body)
	// log.Println(string(a))
	var messages []logging.LogMessage
	if err := json.NewDecoder(r.Body).Decode(&messages); err != nil {
		log.Printf("Parse message error: %s\n", err.Error())
		http.Error(w, fmt.Sprintf(`{"status":false,"error":"Parser message error:%s"}`, err), 400)
		return
	}

	for _, msg := range messages {
		messagesQueue <- msg
	}

	w.WriteHeader(http.StatusOK)
	io.WriteString(w, `{"status":true}`)
}

// logger full info
func FullInfo(w http.ResponseWriter, r *http.Request) {
	data := make(map[string]interface{})

	data["messages"] = messagesLog.Info()

	w.WriteHeader(http.StatusOK)
	w.Write(helpers.JsonEncode(data))
}

// Check service status
func HealthCheck(w http.ResponseWriter, r *http.Request) {
	data := make(map[string]interface{})

	data["data"] = helpers.GetServiceInfo()

	w.WriteHeader(http.StatusOK)
	w.Write(helpers.JsonEncode(data))
}
